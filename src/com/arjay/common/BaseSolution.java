package com.arjay.common;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public abstract class BaseSolution {
    private static final InputStream originalIn = System.in;
    
    protected abstract void runSolution();
    
    private void setParams(String... params) {
        String joined = String.join("\n", params);
        System.setIn(new ByteArrayInputStream(joined.getBytes()));
    }

    protected void runSolutionWithParam(String... params){
        setParams(params);
        runSolution();
        System.setIn(originalIn);
    }
}

