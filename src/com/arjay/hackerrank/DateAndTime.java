package com.arjay.hackerrank;

import java.util.Calendar;

import com.arjay.common.BaseSolution;

public class DateAndTime extends BaseSolution {

    
    public static String findDay(int month, int day, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1, day);
        return String.format("%tA", cal).toUpperCase();
    }

    @Override
    protected void runSolution() {
        System.out.println(DateAndTime.findDay(3,3,1986));
    }
    
    public static void main(String[] args) {
        DateAndTime obj = new DateAndTime();
        obj.runSolution();
    }
}

