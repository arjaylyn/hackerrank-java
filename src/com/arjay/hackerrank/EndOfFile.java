package com.arjay.hackerrank;

import java.util.Scanner;

import com.arjay.common.BaseSolution;

public class EndOfFile extends BaseSolution{

    @Override
    protected void runSolution() {
        Scanner scan = new Scanner(System.in);
        int i = 1;
        while(scan.hasNextLine()){
            System.out.println((i++) +" "+ scan.nextLine());
        }
        scan.close();
    }
    
    public static void main(String[] args){
        EndOfFile x = new EndOfFile();
        x.runSolutionWithParam("Hello world", "I am a file",
                "Read me until end-of-file.");
    }
}
