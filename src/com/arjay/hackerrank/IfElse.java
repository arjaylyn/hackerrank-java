package com.arjay.hackerrank;

import java.util.Scanner;

public class IfElse {
    private static final Scanner scanner = new Scanner(System.in);

    public static void solution() {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        scanner.close();

        if(N < 1 || N > 100){
            System.out.println("Out of Range");
        }
        else if(N % 2 == 1){
            System.out.println("Weird");
        }
        else if(N >= 1 && N <= 5){
            System.out.println("Not Weird");
        }
        else if(N >= 6 && N <= 20){
            System.out.println("Weird");
        }
        else {
            System.out.println("Not Weird");
        }
    }
    public static void main(String[] args) {
        solution();
    }
}
