package com.arjay.hackerrank;

import java.util.Scanner;

import com.arjay.common.BaseSolution;

public class IntToString extends BaseSolution{

    @Override
    protected void runSolution() {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.close();

        String s = Integer.toString(n);
     
        if (n == Integer.parseInt(s)) {
         System.out.println("Good job");
        } else {
         System.out.println("Wrong answer.");
        }
    }
    
    public static void main(String[] args) {
        IntToString obj = new IntToString();
        obj.runSolutionWithParam("2");
    }
}
