package com.arjay.hackerrank;

import java.util.Scanner;

public class LoopsI {
    
    private static final Scanner scanner = new Scanner(System.in);
    public static void solution() {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        scanner.close();

        for(int i = 1; i <= 10; i++){
            System.out.println(N + " x " + i + " = " + (i*N));
        }
    }

    public static void main(String[] args) {
        solution();
    }
}
