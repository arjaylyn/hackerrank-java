package com.arjay.hackerrank;

import java.util.Scanner;

public class LoopsII {
    
    public static void solution() {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int i=0; i<t; i++){
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();

            int m = 1;
            int p = a;
            String s = "";
            for(int x=1; x<=n; x++){
                if(x > 1) {
                    m = m * 2;
                }
                p = p + (m * b);
                s = s + " " + p;
            }

            System.out.println(s.trim());
        }
        in.close();
    }

    public static void main(String[] args) {
        solution();
    }
}
