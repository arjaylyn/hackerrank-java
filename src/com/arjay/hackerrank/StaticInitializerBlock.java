package com.arjay.hackerrank;

import java.util.Scanner;

import com.arjay.common.BaseSolution;

public class StaticInitializerBlock extends BaseSolution {

    public static Scanner scan = new Scanner(System.in);
    public static int B = scan.nextInt();
    public static int H = scan.nextInt();
    public static boolean flag = getFlag(); 
    
    public static boolean getFlag(){
        if(B > 0 && H > 0) {
            return true;
        }
        else{
            System.out.println("java.lang.Exception: Breadth and height must be positive");
            return false;
        }
    }
    
    @Override
    protected void runSolution() {
		if(flag){
			int area=B*H;
			System.out.print(area);
		}
    }

    public static void main(String[] args){
        StaticInitializerBlock x = new StaticInitializerBlock();
        x.runSolution();
    }
}
