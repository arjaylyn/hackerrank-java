package com.arjay.hackerrank;

import java.util.Scanner;

public class StdinAndStdout {
    
    public static void solution() {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        scan.close();

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
    public static void main(String[] args) {
        solution();
    }
}
