package com.arjay.hackerrank;

import java.util.Scanner;

public class StdinAndStdoutII {
    
    public static void solution() {
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        double d = scan.nextDouble();
        scan.nextLine();
        String s = scan.nextLine();
        scan.close();

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }
    public static void main(String[] args) {
        solution();
    }
}
