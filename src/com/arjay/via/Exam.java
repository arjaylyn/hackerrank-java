package com.arjay.via;

import java.util.Scanner;

public class Exam {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        System.out.println("Input number of rows : ");
        int n = scan.nextInt();
        scan.close();

        for(int i=1; i<=n; i++){
            StringBuilder s = new StringBuilder();

            for(int x=1; x<=i; x++){
                s.append(i+"");
            }

            System.out.println(s.toString());
        }
    }
}
