package test.arjay.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;

public abstract class BaseTest {
    protected final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    protected final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final InputStream originalIn = System.in;
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    /**
     * Used when adding param for System.setIn
     * Sample:
     *      return new String[]{"123","23","43"};
     */
    public abstract String[] getParams();

    @Before
    public void setUpStreams() {
        if(getParams() != null){
            System.setIn(getInputStream(getParams()));
        }
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    
    @After
    public void restoreStreams() {
        System.setIn(originalIn);
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    public ByteArrayInputStream getInputStream(String... params) {
        String joined = String.join("\n", params);
        return new ByteArrayInputStream(joined.getBytes());
    }
}
