package test.arjay.hackerrank;

import static org.junit.Assert.assertEquals;

import com.arjay.hackerrank.StdinAndStdout;

import org.junit.Test;

import test.arjay.common.BaseTest;

public class StdinAndStdoutTest extends BaseTest{
    @Test
    public void testSolution(){
        StdinAndStdout.solution();

        String[] outString = outContent.toString().split("\n");
        assertEquals(getParams()[0], outString[0]);
        assertEquals(getParams()[1], outString[1]);
        assertEquals(getParams()[2], outString[2]);
    }

    @Override
    public String[] getParams() {
        return new String[]{"123","23","43"};
    }
}
