package test.arjay.hackerrank;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.arjay.hackerrank.Welcome;

import org.junit.Test;

import test.arjay.common.BaseTest;

public class WelcomeTest extends BaseTest {    
    @Test
    public void testSolution(){
        Welcome.solution();

        String[] outString = outContent.toString().split("\n");
        assertEquals("Hello, World.", outString[0]);
        assertEquals("Hello, Java.", outString[1]);
    }

    @Override
    public String[] getParams() {
        return null;
    }

}
